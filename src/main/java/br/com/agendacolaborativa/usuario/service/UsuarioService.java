package br.com.agendacolaborativa.usuario.service;

import br.com.agendacolaborativa.usuario.entity.Usuario;
import br.com.agendacolaborativa.usuario.repository.UsuarioRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    public void salvarUsuario(Usuario usuario) {
        usuarioRepository.save(usuario);
    }

    public List<Usuario> listarTodos() {
        return usuarioRepository.findAll();
    }

    public long count() {
        return usuarioRepository.count();
    }

    public Usuario pesquisarUsuarioPorId(String id) {
        return usuarioRepository.findByUsuarioId(id);
    }

    public void excluirUsuario(String id) {
        usuarioRepository.deleteById(id);
    }

}