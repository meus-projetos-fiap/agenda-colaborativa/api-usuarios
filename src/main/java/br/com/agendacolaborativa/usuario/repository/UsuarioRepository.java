package br.com.agendacolaborativa.usuario.repository;

import br.com.agendacolaborativa.usuario.entity.Usuario;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UsuarioRepository extends MongoRepository<Usuario, String> {

    Usuario findByUsuarioId(String id);
}