package br.com.agendacolaborativa.usuario.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.GeneratedValue;

@Document(collection = "usuarios")

@Data
public class Usuario {

    @Id
    @GeneratedValue
    private String usuarioId;
    private String nome;
    private String email;
    private String senha;
    private String aniversario;

}
