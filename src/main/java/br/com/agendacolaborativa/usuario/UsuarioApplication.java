package br.com.agendacolaborativa.usuario;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SpringBootApplication
public class UsuarioApplication {
    private static final Logger logger = LoggerFactory.getLogger(UsuarioApplication.class);

    public static void main(String[] args) {
        logger.info("INI-002");
        SpringApplication.run(UsuarioApplication.class, args);
    }

}
