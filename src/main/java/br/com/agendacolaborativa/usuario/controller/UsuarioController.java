package br.com.agendacolaborativa.usuario.controller;

import br.com.agendacolaborativa.usuario.entity.Usuario;
import br.com.agendacolaborativa.usuario.repository.UsuarioRepository;
import br.com.agendacolaborativa.usuario.service.UsuarioService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

    final static Logger logger = LoggerFactory.getLogger(UsuarioController.class);

    @Autowired
    private UsuarioService usuarioService;
    @Autowired
    private UsuarioRepository usuarioRepository;

    @GetMapping
    public List<Usuario> listarTodosUsuarios() {
        return usuarioService.listarTodos();
    }

    @PostMapping
    public ResponseEntity<?> salvarUsuario(@RequestBody Usuario usuario) {
        usuarioService.salvarUsuario(usuario);
        return new ResponseEntity("Student added successfully", HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public void deletarUsuario(@PathVariable String id) {
        logger.info("Delete[" + id + "]");
        usuarioService.excluirUsuario(usuarioService.pesquisarUsuarioPorId(id).getUsuarioId());
    }

    @PutMapping("/{id}")
    public ResponseEntity<Usuario> atualizarUsuario(@PathVariable String id, @RequestBody Usuario usuario) {
        logger.info("Atualizar[" + id + "]");
        logger.info("Atualizar[" + usuario + "]");
        Usuario _usuario = usuarioService.pesquisarUsuarioPorId(id);
        _usuario.setNome(usuario.getNome());
        _usuario.setEmail(usuario.getEmail());
        _usuario.setSenha(usuario.getSenha());
        _usuario.setAniversario(usuario.getAniversario());
        usuarioRepository.save(_usuario);
        return new ResponseEntity<>(_usuario, HttpStatus.OK);
    }

}
